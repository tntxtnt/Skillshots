# Skillshots

[![build status](https://gitlab.com/tntxtnt/Skillshots/badges/master/build.svg)](https://gitlab.com/tntxtnt/Skillshots/commits/master)

An artillery turn-based game (like Worms or Gunbound)

![Screenshot 01](/md/ss01.png)

![Screenshot 02](/md/ss02.png)

![Screenshot 03](/md/ss03.png)

![Screenshot 04](/md/ss04.png)

![Screenshot 05](/md/ss05.png)

![Screenshot 06](/md/ss06.png)

![Screenshot 07](/md/ss07.png)

![Screenshot 08](/md/ss08.png)

#### Docs
[Doxygen generated documentations](https://tntxtnt.gitlab.io/Skillshots/docs/)

#### Dependencies
- SFML 2.4.2
- Boost 1.64.0 (Geometry, Variant, ...)

#### Todo 
- ~~Add map (customable)~~
- ~~Add camera~~
- ~~Add PhysPoint class (drop)~~
- ~~Add Vehicle class (drop, move, customable, alter gun angle)~~
- ~~Add Rocket class~~
- Add shooting feature (~~vehicle shoot~~, shoot with different power)
- More sprites (vehicles, rockets)
- Add sound/music (...)
- Add different rockets types (...)
- Add multiplayer (...)