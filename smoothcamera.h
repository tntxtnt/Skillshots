#ifndef SMOOTHCAMERA_H
#define SMOOTHCAMERA_H

#include "headers.h"
#include "time.h"

///
/// SmoothCamera
///   In-game camera with smooth movement
///
/// Implementation details:
///   - Is a sf::View
///   - Has an additional target (sf::Vector2f)
///   - Move target, linear interpolation (lerp) the camera to the target
///
class SmoothCamera : public sf::View
{
public:
    //-----------------------CAMERA MOVEMENT--------------------------------
    /// Set camera movement amount per second
    /// @param amt New move amount
    /// @return None
    void  setMovePerSec(float amt);

    /// Get camera movement amount per second
    /// @return Move amount per second
    float getMovePerSec()const;

    /// Set camera lerp amount **per fixed updates**
    /// @param amt New lerp amount
    /// @return None
    void  setLerp(float amt);

    /// Get camera lerp amount **per fixed updates**
    /// @return Lerp amount
    float getLerp()const;

    /// Handle camera movement events (UP DOWN LEFT RIGHT arrow key)
    /// @return None
    void  handleMovement();

    /// Camera movement using linear interpolation
    /// @return None
    void  smoothMove();
    //----------------------------------------------------------------------

    //-----------------------------CAMERA ZOOM------------------------------
    /// Zoom in (make objects bigger) by `zoomAmt` (default 2.0f)
    /// @return None
    void  zoomIn();

    /// Zoom out (make objects smaller) by `zoomAmt` (default 2.0f)
    /// @return None
    void  zoomOut();
    //----------------------------------------------------------------------
public: //saved for later use, maybe a follow() method
    void  matchTargetWithCenter()            { target = getCenter(); } //?
    void  setTarget(const sf::Vector2f& tgt) { target = tgt; }
    void  setTarget(float x, float y)        { target.x = x; target.y = y; }
public: //debug
    /// Get camera target position
    /// @return Camera target position
    sf::Vector2f getTarget()const { return target; }
private:
    sf::Vector2f target = sf::Vector2f(0, 0);

    float lerpAmt       = 0.0625f;
    //float timeSinceLastLerp = 0.0f;

    float mps           = 500.0f;
    float snapThreshold = 1.0f;

    float zoomAmt       = 2.0f;
};

#endif // SMOOTHCAMERA_H
