#ifndef CONFIG_H
#define CONFIG_H

#include "headers.h"

class Config
{
    using VariantTypes = boost::variant<
        std::string,
        int32_t,
        uint32_t,
        float
    >;
public:
    struct ImplicitVariantTypes {
        const VariantTypes& val;
        ImplicitVariantTypes(const VariantTypes& val) : val{ val } {}
        operator std::string()const;
        operator int32_t()const;
        operator uint32_t()const;
        operator float()const;
        operator size_t()const;
        operator bool()const;
    };
public:
    bool loadFromFile(const std::string& filename);

    ImplicitVariantTypes get(const std::string& key)const;

    VariantTypes& operator[](const std::string& key);
public:
    static Config global;
private:
    std::map<std::string, VariantTypes> data;
};

#endif // CONFIG_H
