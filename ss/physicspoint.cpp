#include "physicspoint.h"

namespace ss
{

PhysicsPoint::PhysicsPoint(const sf::Vector2f& pos, const sf::Vector2f& vel,
                           const sf::Vector2f gravity, const sf::Vector2f wind,
                           float gravityScale, float windScale)
: pos{pos}, vel{vel}, acc{}, dispPos{pos}, dispVel{vel},
  halfFixedTimeAcc{}, gravityScale{gravityScale}, windScale{windScale}
{
    setGravityWind(gravity, wind);
}

bool PhysicsPoint::hasCollision()const
{
    return groundId >= 0;
}

void PhysicsPoint::setGravityWind(const sf::Vector2f gravity, const sf::Vector2f wind)
{
    acc = gravityScale*gravity + windScale*wind;
    halfFixedTimeAcc = 0.5f * Time::fixedDelta() * acc;
}

void PhysicsPoint::update()
{
    if (hasCollision()) return;
    auto halfDeltaAcc = 0.5f * Time::delta() * acc;
    dispPos += Time::delta() * (dispVel + halfDeltaAcc);
    dispVel += halfDeltaAcc + halfDeltaAcc;
}

void PhysicsPoint::fixedUpdate()
{
    if (hasCollision()) return;
    auto prevPos = pos;
    pos += Time::fixedDelta() * (vel + halfFixedTimeAcc);
    vel += halfFixedTimeAcc + halfFixedTimeAcc;
    for (int cid = 0; cid < (int)colliders.size(); ++cid)
        checkLineCollision(prevPos, cid);
}

void PhysicsPoint::checkLineCollision(const sf::Vector2f& prevPos, int cid)
{
    Linestring2f moveVec{prevPos, pos};
    auto const& grounds = *(colliders[cid]);
    for (int gid = 0; gid < (int)grounds.size(); ++gid)
    {
        auto const& ground = grounds[gid];
        for (int i = 0, j = 1; j < (int)ground.size(); ++i, ++j)
        {
            Linestring2f segment{ground[i], ground[j]};
            auto p = intersection(segment, moveVec); //intersection point(s)
            if (p.empty()) continue; //no collision
            //has collision (pray that they are not the same line)
            if (bg::distance(prevPos, p[0]) < bg::distance(prevPos, pos))
            {
                groundId = gid;
                segmentId = i;
                colliderId = cid;
                vel.x = vel.y = dispVel.x = dispVel.y = 0;
                dispPos = pos = p[0];
            }
        }
    }
}

void PhysicsPoint::addCollider(const MultiRing2f* collider)
{
    colliders.push_back(collider);
}

void PhysicsPoint::incrementSegmentId()
{
    int n = (*colliders[colliderId])[groundId].size();
    segmentId = (segmentId + 1) % n;
    if (segmentId == n - 1) segmentId = 0;
}

void PhysicsPoint::decrementSegmentId()
{
    int n = (*colliders[colliderId])[groundId].size();
    segmentId = (segmentId + n - 1) % n;
    if (segmentId == n - 1) segmentId = n - 2;
}

void PhysicsPoint::setPosition(const sf::Vector2f& pos)
{
    this->pos = dispPos = pos;
}

void PhysicsPoint::setVelocity(const sf::Vector2f& vel)
{
    this->vel = dispVel = vel;
}

int PhysicsPoint::insideColliderIndex()const
{
	for (int cid = 0; cid < (int)colliders.size(); ++cid)
		if (within(pos, *colliders[cid]))
			return cid;
	return -1;
}

bool PhysicsPoint::isInsideCollider()const
{
	return insideColliderIndex() > -1;
}

}
