#ifndef SS_WORLD_H
#define SS_WORLD_H

#include "../headers.h"
#include "terrain.h"
#include "vehicle.h"
#include "prefab.h"

namespace ss
{

class World : public sf::Drawable, sf::Transformable
{
public:
    /// Load terrain map from file
    /// @param filename File name of the map.
    bool loadMap(const std::string& filename);

	/// Set world's gravity
	/// @param gravity Gravity
	void setGravity(const sf::Vector2f& gravity);

	/// Set world's wind speed
	/// @param wind Wind speed
	void setWind(const sf::Vector2f& wind);

    /// Get world's wind speed
    /// @return The world's wind speed
    const sf::Vector2f& getWind()const;

    /// Get world's gravity
    /// @return The world's gravity
    const sf::Vector2f& getGravity()const;

    /// Get world's terrain
    /// @return The world's terrain
    const ss::Terrain& getTerrain()const;

    /// Step to the next world state
    void update();

    /// Register event (not fully handle event)
    /// @param event The event.
    void catchEvent(const sf::Event& event);

    /// Finish handling registered events
    void handleEvents();

    /// Get current selected vehicle
    /// @return The selected vehicle.
    Vehicle& getSelectedVehicle();

    /// Get rockets
    /// @return Rockets.
    const std::list<Rocket>& getRockets()const;

    /// Add a new rocket to this world
    /// @param type The rocket' to be added's type.
    /// @param pos The rocket's initial position.
    /// @param vel The rocket's initial velocity.
    void addRocket(Rocket::Type type, const sf::Vector2f& pos,
                   const sf::Vector2f& vel);

    /// Add a new vehicle to this world
    /// @param type The rocket' to be added's type.
    /// @param pos The rocket's initial position.
    void addVehicle(Vehicle::Type type, const sf::Vector2f& pos);

	/// Set active vehicle
	/// @param index Index of the vehicle that is about to be activated.
	void setActiveVehicle(int index);

	/// Set switch delay
	/// @param amount Approximately delay time in seconds.
	void setSwitchDelay(float amount);
private:
    /// SFML drawing method
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states)const;
private:
	sf::Vector2f gravity{ 0, 0 };
    sf::Vector2f wind{ 0, 0 };
	ss::Terrain terrain{};
	float timeFrameAccumulator{ 0 };

    std::vector<Vehicle> vehicles;
	int selectedVh{ -1 };

    std::list<Rocket> rockets;

	bool endTurn{ false };
	float switchDelay{ 0.5f };
	float switchDelayCounter{ 0 };
};

}

#endif // WORLD_H
