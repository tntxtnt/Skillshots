#include "groundborder.h"

namespace ss
{

// 0 1 2
// 3 X 5
// 6 7 8
const GroundBorder::TraceDir GroundBorder::traceDirs[4][7] = {
    { {5,0}, {8,0}, {7,0}, {6,1}, {1,2}, {3,1}, {2,2} }, //right-downward
    { {7,1}, {6,1}, {3,1}, {0,3}, {5,0}, {1,3}, {8,0} }, //left-downward
    { {1,2}, {2,2}, {5,2}, {8,0}, {3,3}, {7,0}, {0,3} }, //right-upward
    { {3,3}, {0,3}, {1,3}, {2,2}, {7,1}, {5,2}, {6,1} }, //left-upward
};

bool inBound(const sf::Image& image, const sf::Vector2i& p)
{
    return p.x >= 0 && p.x < (int)image.getSize().x &&
           p.y >= 0 && p.y < (int)image.getSize().y;
}

bool isSolidPixel(const sf::Image& image, const sf::Vector2i& p,
                  unsigned char alphaThreshold)
{
    return inBound(image, p) && image.getPixel(p.x, p.y).a > alphaThreshold;
}

bool isBorderPixel(const sf::Image& image, const sf::Vector2i& p,
                   unsigned char alphaThreshold)
{
    if (!isSolidPixel(image, p, alphaThreshold)) return false;
    auto topPix = sf::Vector2i(p.x, p.y - 1);
    auto botPix = sf::Vector2i(p.x, p.y + 1);
    auto lefPix = sf::Vector2i(p.x - 1, p.y);
    auto rigPix = sf::Vector2i(p.x + 1, p.y);
    int neighborCount = isSolidPixel(image, topPix, alphaThreshold) +
                        isSolidPixel(image, lefPix, alphaThreshold) +
                        isSolidPixel(image, botPix, alphaThreshold) +
                        isSolidPixel(image, rigPix, alphaThreshold);
    return neighborCount == 2 || neighborCount == 3;
}

void GroundBorder::traceImageBorder(const sf::Image& image, int startX, int startY,
                              int alphaThreshold, int steps, float maxSegLen)
{
    // Find first left-top point
    sf::Vector2i from, start;
    for (int i = startY; i < (int)image.getSize().y && !ring.size(); ++i)
        for (int j = startX; j < (int)image.getSize().x && !ring.size(); ++j)
            if (isBorderPixel(image, {j,i}, alphaThreshold))
            {
                start = from = {j, i};
                ring.emplace_back(j + 0.5f, i + 0.5f);
            }

    // Walk right
    int nextDir = 0;
    std::set<sf::Vector2i, Vec2iComp> visited;
    for (int step = 0; step < steps; ++step)
    {
        for (auto const& td : traceDirs[nextDir])
        {
            auto npp = td.dir + from; //next pixel's position
            if (isBorderPixel(image, npp, alphaThreshold) &&
                visited.find(npp) == end(visited))
            {
                visited.insert(from);
                from = npp;
                ring.emplace_back(npp.x + 0.5f, npp.y + 0.5f);
                nextDir = td.nextDir;
                break;
            }
        }
    }
    ring.emplace_back(start.x + 0.5f, start.y + 0.5f);

    ring = simplify(ring, maxSegLen);
    bg::correct(ring);
}

sf::VertexArray GroundBorder::drawableBorder(const Ring2f& ring)
{
    sf::Color borderColor = sf::Color::Red;
    sf::VertexArray border(sf::LinesStrip);
    for (auto const& point : ring)
    {
        border.append(sf::Vertex(point, borderColor));
        borderColor = sf::Color::Green;
    }
    return border;
}

sf::VertexArray GroundBorder::drawableBorder()const
{
    return drawableBorder(ring);
}

} //namespace ss
