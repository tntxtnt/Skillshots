#ifndef SS_ROCKET_H
#define SS_ROCKET_H

#include "../headers.h"
#include "physicspoint.h"
#include "../config.h"

namespace ss
{

class Rocket : public sf::Drawable, public sf::Transformable
{
public:
    enum class Type {
        Default
    };
public:
    /// Load rocket from file info
    /// @param filename Vehicle information file name.
    /// @return true if successfully loaded from `filename`
    bool loadFromFile(const std::string& filename);

    /// Move display body to next state after `Time::delta()` seconds
    void update();

    /// Move physics body to next state after `Time::fixedDelta()` seconds
    /// and check collision.
    void fixedUpdate();

    /// Get explosion shape (SFML CircleShape)
    /// @return The explosion shape.
    sf::Shape& getExplosionShape()const;

    /// Check if this rocket hits any collider
    /// @return `true` if this rocket hits any collider, `false` otherwise.
    bool hasCollision()const;

    /// Check if this rocket flies too low => expired => destroy it
    /// @param y The minimum height.
    /// @return `true` if this rocket flies lower than `y`, `false` otherwise.
    bool isFlyingLowerThan(float y)const;

    /// Set gravity and wind
    /// @param gravity The world's gravity.
    /// @param wind The world's wind.
    void setGravityWind(const sf::Vector2f gravity, const sf::Vector2f wind);

    /// Set rocket body position
    /// @param pos Position.
    void setPosition(const sf::Vector2f& pos);

    /// Set rocket body position
    /// @param pos Position.
    const sf::Vector2f& getPosition()const;

    /// Set rocket body velocity
    /// @param vel Velocity.
    void setVelocity(const sf::Vector2f& vel);

    /// Add collider
    /// @param collider Pointer to the collider.
    void addCollider(const MultiRing2f* collider);
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states)const;

    /// Sync `sprite` with `body.dispPos`,
    /// sprite rotation with `body.dispVel`, etc.
    void sync();
private:
    PhysicsPoint body;
    float rotation = 0.0f;
    std::shared_ptr<sf::Texture> pBodyTex = nullptr;
    sf::Sprite bodySpr;
    std::shared_ptr<sf::Shape> pExplosionShape = nullptr;
	bool isInsideCollider = false;
};

}


#endif // ROCKET_H
