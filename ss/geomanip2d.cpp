#include "geomanip2d.h"

// Return lhs - rhs
MultiRing2f difference(Ring2f const& lhs, Ring2f const& rhs)
{
    MultiRing2f out;
    bg::difference(lhs, rhs, out);
    return out;
}

// Return lhs - rhs
MultiRing2f difference(MultiRing2f const& lhs, MultiRing2f const& rhs)
{
    MultiRing2f result;
    for (Ring2f const& p : lhs)
        for (Ring2f const& q : rhs)
        {
            auto out = difference(p, q);
            result.insert(result.end(), out.begin(), out.end());
        }
    return result;
}

// Return lhs - rhs
MultiRing2f difference(MultiRing2f const& lhs, Ring2f const& rhs)
{
    MultiRing2f result;
    for (Ring2f const& p : lhs)
    {
        auto out = difference(p, rhs);
        result.insert(result.end(), out.begin(), out.end());
    }
    return result;
}

bool within(Point2f const& p, MultiRing2f const& rhs)
{
    for (Ring2f const& ring : rhs)
        if (bg::within(p, ring))
            return true;
    return false;
}

// Return a point between p and q
// percentage should be in [0.0, 1.0]
//     0.0 ==> p
//     1.0 ==> q
Point2f point_between(Point2f const& p, Point2f const& q, float percentage)
{
    return p*(1.0f-percentage) + q*percentage;
}

std::ostream& operator<<(std::ostream& out, Point2f const& p)
{
    return out << '(' << p.x << ',' << p.y << ')';
}

std::vector<Point2f> intersection(Ring2f const& ring, Linestring2f const& segments)
{
    std::vector<Point2f> out;
    bg::intersection(ring, segments, out);
    return out;
}

std::vector<Point2f> intersection(MultiRing2f const& rings, Linestring2f const& segments)
{
    std::vector<Point2f> result;
    for (Ring2f const& ring : rings)
    {
        auto out = intersection(ring, segments);
        result.insert(result.end(), out.begin(), out.end());
    }
    return result;
}

std::vector<Point2f> intersection(Linestring2f const& lhs, Linestring2f const& rhs)
{
    std::vector<Point2f> out;
    bg::intersection(lhs, rhs, out);
    return out;
}

Linestring2f simplify(Linestring2f const& line, float dist)
{
    Linestring2f result;
    bg::simplify(line, result, dist);
    return result;
}

Ring2f simplify(Ring2f const& ring, float dist)
{
    Ring2f result;
    bg::simplify(ring, result, dist);
    return result;
}

MultiRing2f simplify(MultiRing2f const& multiring, float dist)
{
    MultiRing2f result;
    for (Ring2f const& ring : multiring)
        result.push_back(simplify(ring, dist));
    return result;
}
