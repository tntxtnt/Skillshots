#ifndef SS_GEOMANIP_2D_H
#define SS_GEOMANIP_2D_H

#include "../headers.h"

namespace bg = boost::geometry;

using Point2f      = sf::Vector2f;
using Linestring2f = bg::model::linestring<Point2f>;
using Ring2f       = bg::model::ring<Point2f>;
using MultiRing2f  = std::vector<Ring2f>;

BOOST_GEOMETRY_REGISTER_POINT_2D(Point2f, float, bg::cs::cartesian, x, y)

// Debug functions
std::ostream& operator<<(std::ostream&, Point2f const&);


// Geometry functions
MultiRing2f difference(Ring2f const&, Ring2f const&);
MultiRing2f difference(MultiRing2f const&, MultiRing2f const&);
MultiRing2f difference(MultiRing2f const&, Ring2f const&);

bool within(Point2f const&, MultiRing2f const&);

Point2f point_between(Point2f const&, Point2f const&, float);

std::vector<Point2f> intersection(Ring2f const&, Linestring2f const&);
std::vector<Point2f> intersection(MultiRing2f const&, Linestring2f const&);
std::vector<Point2f> intersection(Linestring2f const&, Linestring2f const&);

//Simplified geometries
Linestring2f simplify(Linestring2f const&, float);
Ring2f		 simplify(Ring2f const&, float);
MultiRing2f  simplify(MultiRing2f const&, float);

#endif // SS_GEOMANIP_H
