#include "terrain.h"

namespace ss
{

bool Terrain::loadFromFile(const std::string& filename)
{
    Config cnf;
    if (!cnf.loadFromFile(filename))
    {
        std::cerr << "Cannot load config file " << filename << "\n";
        return false;
    }

    sf::Image mapImg;
    if (!mapImg.loadFromFile(cnf.get("pngFileName")))
        return false;
    sf::Texture mapTex;
    if (!mapTex.loadFromImage(mapImg))
        return false;
    rendTex.setSmooth(true);
    rendTex.create(mapTex.getSize().x, mapTex.getSize().y);
    rendTex.draw(sf::Sprite(mapTex));
    rendTex.display();
    for (int i = 0; i < (int)cnf.get("pieceCount"); ++i)
    {
        GroundBorder ground;
        ground.traceImageBorder(mapImg,
                                cnf.get("startX_"+std::to_string(i+1)),
                                cnf.get("startY_"+std::to_string(i+1)),
                                cnf.get("alphaThreshold_"+std::to_string(i+1)),
                                cnf.get("maxSteps_"+std::to_string(i+1)),
                                cnf.get("maxSegLen_"+std::to_string(i+1)));
        std::cout << "Piece " << i+1 << ": " << ground.getBorder().size() << " points\n";
        terrain.push_back(ground.getBorder());
        traceBorders.push_back(ground.drawableBorder());
    }
    colorkey = sf::Color(cnf.get("colorkey"));
    if (!colorkeyShader.loadFromFile(Config::global.get("SHADER:colorkey.frag"),
                                     sf::Shader::Fragment))
        return false;
    colorkeyShader.setUniform("colorkey", sf::Glsl::Vec4(colorkey));
    colorkeyShader.setUniform("texture", sf::Shader::CurrentTexture);

    return true;
}

void Terrain::draw(sf::RenderTarget& target, sf::RenderStates states)const
{
    states.transform *= getTransform();
    target.draw(sf::Sprite(rendTex.getTexture()), &colorkeyShader);
    for (auto const& border : traceBorders)
        target.draw(border);
}

void Terrain::takeExplosion(const sf::Vector2f& at, sf::Shape& explosionShape)
{
    explosionShape.setPosition(at);
    explosionShape.setFillColor(colorkey);

    auto newTerrain = difference(terrain, getRing2f(explosionShape));
    terrain = simplify(newTerrain, 1.0f);
    for (size_t i = 0; i < terrain.size(); ++i)
    {
        std::cout << "Piece " << i+1 << ": " << terrain[i].size() << " points\n";
    }
    traceBorders.clear();
    for (auto& ring : terrain)
        traceBorders.push_back(GroundBorder::drawableBorder(ring));

    rendTex.draw(explosionShape);
    rendTex.display();
}

Ring2f Terrain::getRing2f(const sf::Shape& shape)
{
    Ring2f ring;
    ring.resize(shape.getPointCount() + 1);
    auto tf = shape.getTransform();
    for (size_t i = 0; i < ring.size() - 1; ++i)
        ring[i] = tf.transformPoint(shape.getPoint(i));
    ring[ring.size() - 1] = ring[0];
    bg::correct(ring);
    return ring;
}

int Terrain::size()const
{
    return terrain.size();
}

const MultiRing2f& Terrain::raw()const
{
    return terrain;
}

sf::Color Terrain::getColorkey()const
{
    return colorkey;
}

}
