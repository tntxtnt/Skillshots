#ifndef SS_PREFAB_H
#define SS_PREFAB_H

#include "../headers.h"
#include "rocket.h"
#include "vehicle.h"

namespace  ss
{

class Prefab
{
    Prefab() = delete;
public:
    using Object = boost::variant<Vehicle, Rocket>;
    using Type = boost::variant<Vehicle::Type, Rocket::Type>;
    using Store = std::map<Type, Object>;
public:
    /// Get a clone of a certain rocket type
    /// @param type Rocket type.
    /// @return A copy of rocket type `type`.
    static Rocket clone(Rocket::Type type);

    /// Load a sample rocket from file
    /// @param type Rocket type.
    /// @param filename Name of the file contains rocket resources details.
    /// @return `true` if successfully loaded,
    /// `false` if already loaded or error.
    static bool loadFromFile(Rocket::Type type, const std::string& filename);


    /// Get a clone of a certain vehicle type
    /// @param type Vehicle type.
    /// @return A copy of vehicle type `type`.
    static Vehicle clone(Vehicle::Type type);

    /// Load a sample vehicle from file
    /// @param type Vehicle type.
    /// @param filename Name of the file contains vehicle resources details.
    /// @return `true` if successfully loaded,
    /// `false` if already loaded or error.
    static bool loadFromFile(Vehicle::Type type, const std::string& filename);
private:
    static Store store;
};

} //namespace ss

#endif // ROCKETFACTORY_H
