#ifndef SS_VEHICLE_H
#define SS_VEHICLE_H

#include "../headers.h"
#include "rocket.h"
#include "../config.h"

namespace ss
{

class World;

class Vehicle : public sf::Drawable, public sf::Transformable
{
    friend class World;
    enum class MoveType { None, Left, Right };
    enum class AngleOp  { None, Up, Down };
    enum class ShootType{ None, Powering, Shot };
public:
    enum class Type {
        America
    };
public:
    /// Load vehicle from file info
    /// @param filename Vehicle information file name.
    /// @return true if successfully loaded from `filename`
    bool loadFromFile(const std::string& filename);

    /// Move display body to next state after `Time::delta()` seconds
    void update();

    /// Move physics body to next state after `Time::fixedDelta()` seconds
    /// and check collision.
    void fixedUpdate();

    /// Register event (keyboard arrows)
    /// @param event The event.
    void catchEvent(const sf::Event& event);

    /// Get display position
    /// @return Display position of the vehicle.
    sf::Vector2f displayPos()const { return body.dispPos; }

    /// Set gravity and wind
    /// @param gravity The world's gravity.
    /// @param wind The world's wind.
    void setGravityWind(const sf::Vector2f gravity, const sf::Vector2f wind);

    /// Set vehicle body position
    /// @param pos Position.
    void setPosition(const sf::Vector2f& pos);

    /// Add collider
    /// @param collider Pointer to the collider.
    void addCollider(const MultiRing2f* collider);

    /// Small levitate
    /// @param dy Small vertical amount to levitate
    void levitate(float dy);

    /// Activate this vehicle (enable drawing gun angle)
    void activate();

    /// Deactivate this vehicle (disable drawing gun angle)
    void deactivate();

    /// Move vehicle to the left
    void moveLeft();

    /// Move vehicle to the right
    void moveRight();

    /// Move gun angle up
    void moveGunAngleUp();

    /// Move gun angle down
    void moveGunAngleDown();

    /// Get shot rocket
    /// @return A tuple of rocket's type, rocket's position, rocket's velocity
    std::tuple<Rocket::Type, sf::Vector2f, sf::Vector2f> getRocket()const;

	/// Refill fuel
	void refillFuel();
private:
    /// Sync `sprite` and `hitbox` positions with `body.dispPos`,
    /// groundAngle with segment angle, etc.
    void sync();

    /// Move vehicle to the left
    /// @param amount Move amount
    void moveLeftExact(float amount);

    /// Move vehicle to the right
    /// @param amount Move amount
    void moveRightExact(float amount);
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states)const;
    void flipLeft();
    void flipRight();
    static bool inRange(float degree, const sf::Vector2f& range);
    bool isGrounded()const;
    float getGroundSlope()const;
    float getMoveAmount(float fuelAmount)const;
private:
    PhysicsPoint body{{0,0}, {0,0}, {0,0}, {0,0}, 1.0f, 0.0f};
    bool isActive = false;

    std::shared_ptr<sf::Shape> pBorderShape = nullptr;
    Ring2f hitbox;
    sf::VertexArray traceHitbox{sf::LinesStrip}; //debug
    bool isAtRightEdge = false;
    bool facingRight = true;

    float gunAngle = 0.0f;
    float gunMaxAngle = 0.0f;
    float gunPreciseAngle = 0.0f;
    sf::Vector2f gunRange;
    sf::Vector2f gunPreciseRange;
    sf::Vector2f gunPos;

    float fuelPerMoveAmount = 1.0f;
    float maxFuel = 100.0f;
    float fuel = 100.0f;
    float maxMovePenalty = 3.0f;
    MoveType moving = MoveType::None;
    AngleOp angleOp = AngleOp::None;
    ShootType shooting = ShootType::None;

    Rocket::Type rocketType = Rocket::Type::Default;

    std::shared_ptr<sf::Texture> pBodyTex = nullptr;
    std::shared_ptr<sf::Texture> pAngleTex = nullptr;
    std::shared_ptr<sf::Texture> pGunAngleTex = nullptr;
    sf::Sprite bodySpr;
    sf::Sprite angleSpr;
    sf::Sprite gunAngleSpr;

	bool isInsideCollider = false;
	int  insideColliderIndex = -1;
};

}

#endif // SS_VEHICLE_H
