#ifndef SS_PHYSICS_POINT_H
#define SS_PHYSICS_POINT_H

#include "../headers.h"
#include "../time.h"
#include "geomanip2d.h"
#include <vector>

namespace ss
{

class Vehicle;

class PhysicsPoint
{
    friend class Vehicle;
public:
    /// Ctor
    /// @param pos Initial position in world.
    /// @param vel Initial velocity.
    /// @param gravity World's gravity.
    /// @param wind World's wind.
    /// @param gravityScale Gravity scale (usually 1.0f).
    /// @param windScale Wind scale.
    PhysicsPoint(const sf::Vector2f& pos={0,0}, const sf::Vector2f& vel={0,0},
                 const sf::Vector2f gravity={0,0}, const sf::Vector2f wind={0,0},
                 float gravityScale=1.0f, float windScale=1.0f);

    /// Move display point to next state after `Time::delta()` seconds
    void update();

    /// Move physics point to next state after `Time::fixedDelta()` seconds
    /// and check collision.
    void fixedUpdate();

    /// Check if this point has collided with something
    /// @return `true` if this point has collide with terrain or player, `false` otherwise
    bool hasCollision()const;

	/// Check if this point is inside any of the colliders
	/// @return `true` if so, `false` otherwise.
	bool isInsideCollider()const;

	/// Get the inside collider index
	/// @return Collider index, -1 if there is none.
	int insideColliderIndex()const;
public: //setters
    /// Set gravity and wind. Should only be called when the world's
    /// gravity and/or wind changed.
    /// @param gravity The world's gravity
    /// @param wind The world's wind
    void setGravityWind(const sf::Vector2f gravity, const sf::Vector2f wind);

    /// Add collider
    /// @param collider Pointer to the collider.
    void addCollider(const MultiRing2f* collider);

    /// Set position
    /// @param pos New position.
    void setPosition(const sf::Vector2f& pos);

    /// Set velocity
    /// @param vel New velocity.
    void setVelocity(const sf::Vector2f& vel);
public: //getters & setters
    const sf::Vector2f& getPos()const { return pos; }
    const sf::Vector2f& getVel()const { return vel; }
    const sf::Vector2f& getDispPos()const { return dispPos; }
    const sf::Vector2f& getDispVel()const { return dispVel; }
    int getGroundId()const  { return groundId; }
    int getSegmentId()const { return segmentId; }
    const MultiRing2f* getCollider()const
    { return colliderId == -1 ? nullptr : colliders[colliderId]; }
    void incrementSegmentId();
    void decrementSegmentId();
private:
    /// Check line-line collision
    /// @param prevPos Previous position of this point.
    /// @param cid Index of the collider to check with
    void checkLineCollision(const sf::Vector2f& prevPos, int cid);
private:
    sf::Vector2f pos;
    sf::Vector2f vel;
    sf::Vector2f acc;
    sf::Vector2f dispPos;
    sf::Vector2f dispVel;
    sf::Vector2f halfFixedTimeAcc;
    int groundId = -1;
    int segmentId = -1;
    int colliderId = -1;
    float gravityScale = 1.0f;
    float windScale = 1.0f;
    std::vector<const MultiRing2f*> colliders;
};

}

#endif // PHYSICS_POINT_H
