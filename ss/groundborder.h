#ifndef SS_GROUNDBORDER_H
#define SS_GROUNDBORDER_H

#include "../headers.h"
#include "geomanip2d.h"

namespace ss
{

class GroundBorder
{
private: //helper structs
    struct TraceDir {
        sf::Vector2i dir;
        int nextDir;
        TraceDir(int n, int nDir) : dir(n%3-1, n/3-1), nextDir(nDir) {}
    };
    static const TraceDir traceDirs[4][7];
    struct Vec2iComp {
        bool operator()(const sf::Vector2i& lhs, const sf::Vector2i& rhs)
        { return lhs.x == rhs.x ? lhs.y < rhs.y : lhs.x < rhs.x; }
    };
public:
    /// Trace border of an image and store it in `ring`
    /// @param image The map image
    /// @param startX x-coord of scanning pixel (generally inside ground piece)
    /// @param startY y-coord of scanning pixel
    /// @param alphaThreshold 0-254 Alpha threshold of a pixel to be considered
    ///   valid ground pixel. If the result ring is jagged, try increasing this
    ///   valid.
    /// @param steps (Overestimated) Number of points on the border
    /// @param maxSegLen Max segment length. Generally 1.0f, since
    ///   boost::simplify will "over" optimize it.
    /// @return None
    void traceImageBorder(const sf::Image& image, int startX, int startY,
                          int alphaThreshold=0,  int steps=10000,
                          float maxSegLen=1.0f);

    /// Border getter
    /// @return the ground's border
    Ring2f getBorder()const { return ring; }

    /// Get SFML drawable border
    /// @return A drawable VertexArray of the ground's border
    sf::VertexArray drawableBorder()const;

    /// Get SFML drawable border from a Ring2f
    /// @param ring Input Ring2f.
    /// @return A drawable VertexArray of `ring`.
    static sf::VertexArray drawableBorder(const Ring2f& ring);
private:
    Ring2f ring;
};

} //namespace ss

#endif // SS_GROUNDBORDER_H
