#ifndef SS_TERRAIN_H
#define SS_TERRAIN_H

#include "../headers.h"
#include "groundborder.h"
#include "../config.h"

namespace ss
{

class Terrain : public sf::Drawable, public sf::Transformable
{
public:
    /// Load terrain from map info file
    /// @param filename Map info file name
    /// @return true if successfully loaded terrain, false otherwise.
    bool loadFromFile(const std::string& filename);

    /// Get piece count
    /// @return Piece count in the terrain
    int size()const;

    /// Get the actual terrain
    /// @return The actual terrain.
    const MultiRing2f& raw()const;

    /// Take the explosion from a rocket
    /// @param at Position of the explosion.
    /// @param explosionShape The exploding shape.
    void takeExplosion(const sf::Vector2f& at, sf::Shape& explosionShape);

    /// Get colorkey of this map
    /// @return Colorkey of this map.
    sf::Color getColorkey()const;
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states)const;

    /// Get Ring2f representation of a shape
    /// @param shape Any shape.
    /// @return Ring2f representation of `shape`.
    static Ring2f getRing2f(const sf::Shape& shape);
private:
    MultiRing2f terrain;
    sf::RenderTexture rendTex;
    std::vector<sf::VertexArray> traceBorders;
    sf::Shader colorkeyShader;
    sf::Color colorkey;
};


} //namespace ss

#endif // SS_TERRAIN_H
