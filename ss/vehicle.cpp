#include "vehicle.h"

namespace ss
{

bool Vehicle::loadFromFile(const std::string& filename)
{
    Config cnf;
    if (!cnf.loadFromFile(filename))
    {
        std::cerr << "Cannot load config file " << filename << "\n";
        return false;
    }

    pBodyTex = std::make_shared<sf::Texture>();
    if (!pBodyTex->loadFromFile(cnf.get("pngFileName")))
    {
        std::cerr << "Cannot open " << (std::string)cnf.get("pngFileName") << "\n";
        return false;
    }
    pBodyTex->setSmooth(true);
    bodySpr.setTexture(*pBodyTex);

    facingRight = cnf.get("facingRight");

    if ((std::string)cnf.get("borderShapeType") == "CircleShape")
    {
        pBorderShape = std::make_shared<sf::CircleShape>(
                           cnf.get("borderRadius"),
                           cnf.get("borderPointCount"));
        pBorderShape->setScale(cnf.get("scaleX"), cnf.get("scaleY"));
    }
    bodySpr.setOrigin(cnf.get("originX"), cnf.get("originY"));
    pBorderShape->setOrigin(bodySpr.getOrigin());

    hitbox.resize(pBorderShape->getPointCount() + 1);
    sf::Color color = sf::Color::Green;
    for (auto const& point : hitbox)
    {
        traceHitbox.append(sf::Vertex(point, color));
        color = sf::Color::Red;
    }

    gunPos.x = cnf.get("gunPosX");
    gunPos.y = cnf.get("gunPosY");

    // Sprite angle circle drawing
    float r1 = cnf.get("outerAngleRadius");
    float tx = 2*r1, ty = tx;
    sf::RenderTexture rendTexAngle1;
    if (!rendTexAngle1.create(tx, ty)) return false;

    sf::CircleShape circle{r1-0.5f};
    circle.setOrigin(circle.getRadius(), circle.getRadius());
    circle.setPosition(r1, r1);
    circle.setFillColor(sf::Color{cnf.get("angleCircleColor")});
    sf::RectangleShape vLine{sf::Vector2f{2.0f, r1}};
    vLine.setFillColor(sf::Color{cnf.get("angleCircleLineColor")});
    vLine.setPosition(r1-1, 0);
    sf::RectangleShape hLine{sf::Vector2f{r1, 2.0f}};
    hLine.setFillColor(sf::Color{cnf.get("angleCircleLineColor")});
    hLine.setPosition(r1, r1-1);

    sf::Texture texRing;
    if (!texRing.loadFromFile(cnf.get("ringBorderFileName"))) return false;

    sf::Sprite sprRing;
    sprRing.setTexture(texRing);

    rendTexAngle1.draw(circle);
    rendTexAngle1.draw(vLine);
    rendTexAngle1.draw(hLine);
    rendTexAngle1.draw(sprRing);
    rendTexAngle1.display();

    // Sprite angle gun angle drawing
    gunMaxAngle = cnf.get("gunOuterAngle");
    gunPreciseAngle = cnf.get("gunInnerAngle");

    sf::RenderTexture rendTexAngle2;
    if (!rendTexAngle2.create(tx, ty)) return false;
    sf::RenderTexture rendTexAngle3;
    if (!rendTexAngle3.create(tx, ty)) return false;

    float r2 = cnf.get("gunInnerAngleRadius");
    float r3 = cnf.get("gunOuterAngleRadius");
    sf::CircleShape gunCirc{r3};
    gunCirc.setFillColor(sf::Color::Transparent);
    gunCirc.setOutlineColor(sf::Color{cnf.get("gunOuterAngleColor")});
    gunCirc.setOutlineThickness(r2 - r3);
    gunCirc.setOrigin(r3, r3);
    gunCirc.setPosition(r1, r1);
    sf::RectangleShape vRect{sf::Vector2f{r1, ty}};
    vRect.setFillColor(sf::Color::Red);
    sf::RectangleShape hRect{sf::Vector2f{tx, r1}};
    hRect.setFillColor(sf::Color::Red);
    hRect.setPosition(0, r1);
    sf::RectangleShape aRect{sf::Vector2f{r1, r1}};
    aRect.setFillColor(sf::Color::Red);
    aRect.rotate(45 - gunMaxAngle/2);
    aRect.setOrigin(r1, r1);
    aRect.setPosition(r1, r1);

    rendTexAngle2.draw(gunCirc);
    rendTexAngle2.draw(vRect);
    rendTexAngle2.draw(hRect);
    rendTexAngle2.draw(aRect);
    aRect.rotate(gunMaxAngle);
    aRect.setOrigin(0, r1);
    rendTexAngle2.draw(aRect);
    rendTexAngle2.display();

    sf::RectangleShape bRect{sf::Vector2f{r1, r1}};
    bRect.setFillColor(sf::Color::Red);
    bRect.rotate(45 - gunPreciseAngle/2);
    bRect.setOrigin(r1, r1);
    bRect.setPosition(r1, r1);


    sf::CircleShape gunCircInner{r3};
    gunCircInner.setFillColor(sf::Color::Transparent);
    gunCircInner.setOutlineColor(sf::Color{cnf.get("gunInnerAngleColor")});
    gunCircInner.setOutlineThickness(r2 - r3);
    gunCircInner.setOrigin(r3, r3);
    gunCircInner.setPosition(r1, r1);

    rendTexAngle3.draw(gunCircInner);
    rendTexAngle3.draw(vRect);
    rendTexAngle3.draw(hRect);
    rendTexAngle3.draw(bRect);
    bRect.rotate(gunPreciseAngle);
    bRect.setOrigin(0, r1);
    rendTexAngle3.draw(bRect);
    rendTexAngle3.display();

    // Get sprite angle
    sf::RenderTexture rendTexAngle;
    if (!rendTexAngle.create(tx, ty)) return false;

    sf::Shader shdRedFilter;
    if (!shdRedFilter.loadFromFile(Config::global.get("SHADER:colorkey.frag"),
                                   sf::Shader::Fragment))
        return false;
    shdRedFilter.setUniform("colorkey", sf::Glsl::Vec4(sf::Color::Red));
    shdRedFilter.setUniform("texture", sf::Shader::CurrentTexture);

    sf::Texture texRadial;
    if (!texRadial.loadFromFile(cnf.get("alphaMaskFileName"))) return false;

    sf::Shader shdAlphaMask;
    if (!shdAlphaMask.loadFromFile(Config::global.get("SHADER:alphamask.frag"),
                                   sf::Shader::Fragment))
        return false;
    shdAlphaMask.setUniform("masktex", texRadial);
    shdAlphaMask.setUniform("texture", sf::Shader::CurrentTexture);

    rendTexAngle.draw(sf::Sprite{rendTexAngle1.getTexture()}, &shdAlphaMask);
    rendTexAngle.draw(sf::Sprite{rendTexAngle2.getTexture()}, &shdRedFilter);
    rendTexAngle.draw(sf::Sprite{rendTexAngle3.getTexture()}, &shdRedFilter);
    rendTexAngle.display();

    pAngleTex = std::make_shared<sf::Texture>(rendTexAngle.getTexture());
    pAngleTex->setSmooth(true);
    angleSpr.setTexture(*pAngleTex);
    angleSpr.setOrigin(circle.getRadius(), circle.getRadius());

    // Draw gun angle indicator
    pGunAngleTex = std::make_shared<sf::Texture>();
    if (!pGunAngleTex->loadFromFile(cnf.get("gunIndicatorFileName")))
        return false;
    pGunAngleTex->setSmooth(true);
    gunAngleSpr.setTexture(*pGunAngleTex);
    gunAngleSpr.setOrigin(pGunAngleTex->getSize().x / 2, r3);

    // Gun angle calculations
    // gunAngle = [0..90], 0 = vertical, 90 = horizontal
    gunAngle = 45.0f;
    // gunRange min/max possible gunAngle
    gunRange.x = 45.0f - gunMaxAngle / 2.0f;
    gunRange.y = gunRange.x + gunMaxAngle;
    // gunRange min/max possible precise angle
    gunPreciseRange.x = 45.0f - gunPreciseAngle / 2.0f;
    gunPreciseRange.y = gunPreciseRange.x + gunPreciseAngle;


    fuelPerMoveAmount = cnf.get("fuelPerMoveAmount");
    fuel = maxFuel = cnf.get("maxFuel");
    maxMovePenalty = cnf.get("maxMovePenalty");

    return true;
}

void Vehicle::sync()
{
    //sync pBorderShape, sprite with dispPos
    bodySpr.setPosition(body.dispPos);
    pBorderShape->setPosition(bodySpr.getPosition());

    //sync shape's angle with ground angle
    float groundAngle = bodySpr.getRotation();
    if (isGrounded()) //this make sure body.getCollider() doesn't return nullptr
    {
        groundAngle = std::atan(getGroundSlope()) / M_PI * 180;
    }
    else
    {
        float zero = 0;
        float full = 360;
        float da = Config::global.get("decrAnglePerFrame");
        if (groundAngle - zero < full - groundAngle)
        {
            groundAngle -= da;
            if (groundAngle < zero) groundAngle = zero;
        }
        else
        {
            groundAngle += da;
            if (groundAngle > full) groundAngle = full;
        }
    }
    bodySpr.setRotation(groundAngle);
    pBorderShape->setRotation(bodySpr.getRotation());
    angleSpr.setRotation(bodySpr.getRotation());
    float gunIndicatorAngle = bodySpr.getRotation();
    if (facingRight) gunIndicatorAngle += gunAngle;
    else             gunIndicatorAngle -= gunAngle;
    gunAngleSpr.setRotation(gunIndicatorAngle);

    //sync hitbox, traceHitbox with pBorderShape
    auto transform = pBorderShape->getTransform();
    for (size_t i = 0; i < hitbox.size() - 1; ++i)
        traceHitbox[i].position = hitbox[i] = transform.transformPoint(pBorderShape->getPoint(i));
    traceHitbox[hitbox.size() - 1].position = hitbox[hitbox.size() - 1] = hitbox[0];

    //sync angleSpr
    sf::Vector2f gp = bodySpr.getPosition() + gunPos;
    transform = sf::Transform{};
    transform.rotate(bodySpr.getRotation(), bodySpr.getPosition());
    angleSpr.setPosition(transform.transformPoint(gp));
    gunAngleSpr.setPosition(angleSpr.getPosition());
}

void Vehicle::draw(sf::RenderTarget& target, sf::RenderStates states)const
{
    states.transform *= getTransform();
    target.draw(bodySpr);
    if (isActive)
    {
        target.draw(angleSpr);
        target.draw(gunAngleSpr);
    }
    target.draw(traceHitbox);
}

void Vehicle::update()
{
    if (isGrounded() || isInsideCollider) return;
    body.update();
    sync();
}

void Vehicle::fixedUpdate()
{
    if (isGrounded()) return;
	if (!isInsideCollider)
	{
		insideColliderIndex = body.insideColliderIndex();
		isInsideCollider = insideColliderIndex > -1;
	}
	if (!isInsideCollider)
	{
		body.fixedUpdate();
		if (body.hasCollision())
		{
			if (isAtRightEdge) body.incrementSegmentId();
			sync();
		}
	}
	else
	{
		//vehicle is inside a terrain!
		//make a super long upward vertical line to get nearest collison
		Point2f above = body.getPos();
		above.y = -1000;
		body.checkLineCollision(above, insideColliderIndex);
		isInsideCollider = false;
		insideColliderIndex = -1;
		sync();
	}
}

void Vehicle::catchEvent(const sf::Event& event)
{
    if (!isGrounded())
    {
        moving = MoveType::None;
        angleOp = AngleOp::None;
        shooting = ShootType::None;
    }
    else if (event.type == sf::Event::KeyPressed)
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) ||
            sf::Keyboard::isKeyPressed(sf::Keyboard::RControl))
        {
            moving = MoveType::None;
            angleOp = AngleOp::None;
            shooting = ShootType::None;
            return;
        }
        if (event.key.code == sf::Keyboard::Left)
            moving = MoveType::Left;
        else if (event.key.code == sf::Keyboard::Right)
            moving = MoveType::Right;
        else if (event.key.code == sf::Keyboard::Up)
            angleOp = AngleOp::Up;
        else if (event.key.code == sf::Keyboard::Down)
            angleOp = AngleOp::Down;
        else if (event.key.code == sf::Keyboard::Space)
            shooting = ShootType::Powering;
    }
    else if (event.type == sf::Event::KeyReleased)
    {
        if (event.key.code == sf::Keyboard::Space)
            shooting = ShootType::Shot;
    }
}

void Vehicle::flipLeft()
{
    facingRight = false;
    bodySpr.setTextureRect(sf::IntRect{
        (int)pBodyTex->getSize().x, 0, -(int)pBodyTex->getSize().x, (int)pBodyTex->getSize().y
    });
    gunPos.x = -gunPos.x;
    angleSpr.setTextureRect(sf::IntRect{
        (int)pAngleTex->getSize().x, 0, -(int)pAngleTex->getSize().x, (int)pAngleTex->getSize().y
    });
    gunAngleSpr.setTextureRect(sf::IntRect{
        (int)pGunAngleTex->getSize().x, 0, -(int)pGunAngleTex->getSize().x, (int)pGunAngleTex->getSize().y
    });
    bodySpr.setOrigin(pBodyTex->getSize().x - bodySpr.getOrigin().x,
                     bodySpr.getOrigin().y);
    pBorderShape->setOrigin(bodySpr.getOrigin());
    sync();
}

void Vehicle::flipRight()
{
    facingRight = true;
    bodySpr.setTextureRect(sf::IntRect{
        0, 0, (int)pBodyTex->getSize().x, (int)pBodyTex->getSize().y
    });
    gunPos.x = -gunPos.x;
    angleSpr.setTextureRect(sf::IntRect{
        0, 0, (int)pAngleTex->getSize().x, (int)pAngleTex->getSize().y
    });
    gunAngleSpr.setTextureRect(sf::IntRect{
        0, 0, (int)pGunAngleTex->getSize().x, (int)pGunAngleTex->getSize().y
    });
    bodySpr.setOrigin(pBodyTex->getSize().x - bodySpr.getOrigin().x,
                     bodySpr.getOrigin().y);
    pBorderShape->setOrigin(bodySpr.getOrigin());
    sync();
}

void Vehicle::moveLeftExact(float amount)
{
    if (!isGrounded()) return;
    isAtRightEdge = false;

    auto const& ground = (*body.getCollider())[body.getGroundId()];
    int i = body.segmentId;
    int j = (body.segmentId + 1) % ground.size();
    if (ground[i].x > ground[j].x) //if vehicle can land on this segment
    {
        Point2f moveToPoint = ground[j];
        float dist = bg::distance(moveToPoint, body.pos);
        if (dist > amount) //can move within the segment
        {
            body.pos = body.dispPos = point_between(body.pos, moveToPoint,
                                                    amount / dist);
        }
        else
        {
            body.pos = body.dispPos = moveToPoint; //move to the end point
            body.incrementSegmentId();
            moveLeftExact(amount - dist); //recursively move left
        }
    }
    else if (ground[i].y <= ground[j].y) //fall if this segment is downward
    {
        body.groundId = -1;
        body.pos.x = body.dispPos.x = body.pos.x - amount;
    }
    else //upside down segment, change segmentId back
    {
        body.decrementSegmentId();
    }

    sync();
}

void Vehicle::moveRightExact(float amount)
{
    if (!isGrounded()) return;

    auto const& ground = (*body.getCollider())[body.getGroundId()];
    int i = body.segmentId;
    int j = (body.segmentId + 1) % ground.size();
    if (ground[i].x > ground[j].x) //if vehicle can land on this segment
    {
        Point2f moveToPoint = ground[i];
        float dist = bg::distance(moveToPoint, body.pos);
        if (dist > amount) //can move within the segment
        {
            body.pos = body.dispPos = point_between(body.pos, moveToPoint,
                                                    amount / dist);
        }
        else
        {
            body.pos = body.dispPos = moveToPoint; //move to the end point
            body.decrementSegmentId();
            moveRightExact(amount - dist); //recursively move right
        }
    }
    else if (ground[i].y >= ground[j].y) //fall if this segment is upward
    {
        body.groundId = -1;
        body.pos.x = body.dispPos.x = body.pos.x + amount;
    }
    else //upside down segment, change segmentId back
    {
        body.incrementSegmentId();
        isAtRightEdge = true;
    }

    sync();
}

bool Vehicle::inRange(float degree, const sf::Vector2f& range)
{
    return range.x <= degree && degree <= range.y;
}

void Vehicle::setGravityWind(const sf::Vector2f gravity, const sf::Vector2f wind)
{
    body.setGravityWind(gravity, wind);
}

void Vehicle::addCollider(const MultiRing2f* collider)
{
    body.addCollider(collider);
}

void Vehicle::levitate(float dy)
{
    body.groundId = body.segmentId = body.colliderId = -1;
    body.pos.y -= dy;
}

void Vehicle::activate()
{
    isActive = true;
}

void Vehicle::deactivate()
{
    isActive = false;
}

void Vehicle::setPosition(const sf::Vector2f& pos)
{
    body.setPosition(pos);
}

bool Vehicle::isGrounded()const
{
    return body.hasCollision();
}

float Vehicle::getGroundSlope()const
{
    auto const& ground = (*body.getCollider())[body.getGroundId()];
    int i = body.segmentId;
    int j = (body.segmentId + 1) % ground.size();
    return (ground[i].y - ground[j].y) / (ground[i].x - ground[j].x);
}

float Vehicle::getMoveAmount(float fuelAmount)const
{
    float angle = std::atan(std::abs(getGroundSlope())) / M_PI * 180;
    float movePenalty = 1 + angle / 90 * (maxMovePenalty - 1);
    return fuelAmount / movePenalty;
}

void Vehicle::moveLeft()
{
    if (facingRight) return flipLeft();
    float fuelAmount = std::min(fuelPerMoveAmount, fuel);
    if (fuelAmount <= 0) return;
    fuel -= fuelAmount;
    return moveLeftExact(getMoveAmount(fuelAmount));
}

void Vehicle::moveRight()
{
    if (!facingRight) return flipRight();
    float fuelAmount = std::min(fuelPerMoveAmount, fuel);
    if (fuelAmount <= 0) return;
    fuel -= fuelAmount;
    return moveRightExact(getMoveAmount(fuelAmount));
}

void Vehicle::moveGunAngleUp()
{
    gunAngle -= 1.0f;
    if (!inRange(gunAngle, gunRange)) gunAngle += 1.0f;
    else sync();
}

void Vehicle::moveGunAngleDown()
{
    gunAngle += 1.0f;
    if (!inRange(gunAngle, gunRange)) gunAngle -= 1.0f;
    else sync();
}

std::tuple<Rocket::Type, sf::Vector2f, sf::Vector2f> Vehicle::getRocket()const
{
    auto tf = sf::Transform{};
    tf.rotate(bodySpr.getRotation(), bodySpr.getPosition());
    auto rkPos = tf.transformPoint(bodySpr.getPosition() + gunPos);

    float r1 = bodySpr.getRotation();
    float r2 = gunAngle;
    sf::Vector2f dir{1,0};
    if (!facingRight)
    {
        r1 += 180;
        r2 = -r2;
        dir = -dir;
    }

    float rkAngle = r1 + r2 - 90;
    tf = sf::Transform{};
    tf.rotate(rkAngle);
    auto rkDir = tf.transformPoint(dir);

    float rkPower = 200.0f;

    return std::make_tuple(rocketType, rkPos, rkDir * rkPower);
}

void Vehicle::refillFuel()
{
	fuel = maxFuel;
}

}
