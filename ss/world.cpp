#include "world.h"

namespace ss
{

bool World::loadMap(const std::string& filename)
{
    return terrain.loadFromFile(filename);
}

void World::draw(sf::RenderTarget& target, sf::RenderStates states)const
{
    states.transform *= getTransform();
    target.draw(terrain);

    for (auto& vehicle : vehicles) target.draw(vehicle);
    for (auto& rocket  : rockets)  target.draw(rocket);
}

void World::update()
{
    // Display state (update)
    for (auto& vehicle : vehicles) vehicle.update();
    for (auto& rocket  : rockets)  rocket.update();

    // Physics state (fixed update)
    for (timeFrameAccumulator += Time::delta();
         timeFrameAccumulator >= Time::fixedDelta();
         timeFrameAccumulator -= Time::fixedDelta())
    {
        for (auto& vehicle : vehicles) vehicle.fixedUpdate();
        for (auto& rocket  : rockets)  rocket.fixedUpdate();

        // Explosions
        bool hasExplosion = false;
        for (auto& rocket : rockets) if (rocket.hasCollision())
        {
            terrain.takeExplosion(rocket.getPosition(),
                                  rocket.getExplosionShape());
            hasExplosion = true;
        }
        // Remove exploded rockets or rockets that fly out of window
        rockets.remove_if([](auto const& rocket){
            return rocket.hasCollision() || rocket.isFlyingLowerThan(2000);
        });
        if (hasExplosion)
        {
            float dy = 25.f*Time::fixedDelta()*Time::fixedDelta()*gravity.y; //100*old dy
            for (auto& vehicle : vehicles) if (vehicle.isGrounded())
                vehicle.levitate(dy);  //1.0 seems to cause vehicle move through??
                //with g = 200, dt = 0.02
                //0.5gt^2 = 1 => t^2 = 0.01 => t = 0.1 = 5*dt
                //problem when dt divides t??

                //new bug: max angle explosion at 550 480
               //probably because of bg::simplify
        }

		// Switch vehicle
		if (endTurn && rockets.empty())
		{
			switchDelayCounter += Time::fixedDelta();
			if (switchDelayCounter >= switchDelay)
			{
				switchDelayCounter = 0;
				endTurn = false;

				vehicles[selectedVh].refillFuel();
				//switch to next priority vehicle (currently fcfs)
				setActiveVehicle((selectedVh + 1) % vehicles.size());
				//check valid state (alive)
				//...
			}
		}
    }
}

void World::setWind(const sf::Vector2f& wind)
{
	this->wind = wind;
}

void World::setGravity(const sf::Vector2f& gravity)
{
	this->gravity = gravity;
}

const sf::Vector2f& World::getWind()const
{
    return wind;
}

const sf::Vector2f& World::getGravity()const
{
    return gravity;
}

const Terrain& World::getTerrain()const
{
    return terrain;
}

void World::catchEvent(const sf::Event& event)
{
	if (endTurn) return;

    vehicles[selectedVh].catchEvent(event);
}

void World::handleEvents()
{
	if (endTurn) return;

    auto& vh = getSelectedVehicle();

    if (vh.isGrounded())
    {
        if (vh.moving == Vehicle::MoveType::Left)
            vh.moveLeft();
        else if (vh.moving == Vehicle::MoveType::Right)
            vh.moveRight();
        if (vh.angleOp == Vehicle::AngleOp::Up)
            vh.moveGunAngleUp();
        else if (vh.angleOp == Vehicle::AngleOp::Down)
            vh.moveGunAngleDown();

        if (vh.shooting == Vehicle::ShootType::Powering)
        {

        }
        else if (vh.shooting == Vehicle::ShootType::Shot)
        {
            auto rkInfo = vh.getRocket();
            auto rkType = std::get<0>(rkInfo);
            auto rkPos  = std::get<1>(rkInfo);
            auto rkVel  = std::get<2>(rkInfo);
            addRocket(rkType, rkPos, rkVel);
			endTurn = true;
        }
    }
    vh.moving = Vehicle::MoveType::None;
    vh.angleOp = Vehicle::AngleOp::None;
    vh.shooting = Vehicle::ShootType::None;
}

void World::addRocket(Rocket::Type type, const sf::Vector2f& pos,
                      const sf::Vector2f& vel)
{
    Rocket rk = Prefab::clone(Rocket::Type::Default);
    rk.setPosition(pos);
    rk.setVelocity(vel);
    rk.setGravityWind(gravity, wind);
    rk.addCollider(&terrain.raw());
    rockets.emplace_back(std::move(rk));
}

void World::addVehicle(Vehicle::Type type, const sf::Vector2f& pos)
{
    Vehicle vh = Prefab::clone(Vehicle::Type::America);
    vh.setPosition(pos);
    vh.setGravityWind(gravity, wind);
    vh.addCollider(&terrain.raw());
    vehicles.emplace_back(std::move(vh));
}

void World::setActiveVehicle(int index)
{
	if (vehicles.empty())
	{
		selectedVh = -1;
		return;
	}
	if (selectedVh != -1)
		vehicles[selectedVh].deactivate();
	selectedVh = index;
	vehicles[selectedVh].activate();
}

Vehicle& World::getSelectedVehicle()
{
    return vehicles[selectedVh];
}

const std::list<Rocket>& World::getRockets()const
{
    return rockets;
}

void World::setSwitchDelay(float amount)
{
	switchDelay = amount;
}

}
