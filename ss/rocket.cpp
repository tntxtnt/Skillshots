#include "rocket.h"

namespace ss
{

bool Rocket::loadFromFile(const std::string& filename)
{
    Config cnf;
    if (!cnf.loadFromFile(filename))
    {
        std::cerr << "Cannot load config file " << filename << "\n";
        return false;
    }

    pBodyTex = std::make_shared<sf::Texture>();
    if (!pBodyTex->loadFromFile(cnf.get("pngFileName")))
    {
        std::cerr << "Cannot open " << (std::string)cnf.get("pngFileName") << "\n";
        return false;
    }
    pBodyTex->setSmooth(true);
    bodySpr.setTexture(*this->pBodyTex);
    bodySpr.setOrigin(cnf.get("originX"), cnf.get("originY"));
    if ((std::string)cnf.get("explosionShapeType") == "CircleShape")
    {
        float r = cnf.get("borderRadius");
        pExplosionShape = std::make_shared<sf::CircleShape>(
                              r, cnf.get("borderPointCount"));
        pExplosionShape->setOrigin(r, r);
        pExplosionShape->setScale(cnf.get("scaleX"), cnf.get("scaleY"));
    }
    return true;
}

void Rocket::draw(sf::RenderTarget& target, sf::RenderStates states)const
{
    states.transform *= getTransform();
    target.draw(bodySpr);
}

void Rocket::update()
{
    if (hasCollision()) return;
    body.update();
    sync();
}

void Rocket::fixedUpdate()
{
    if (hasCollision()) return;
	isInsideCollider = body.isInsideCollider();
    if (!hasCollision()) body.fixedUpdate();
    if (hasCollision()) sync();
}

void Rocket::sync()
{
    //sync sprite with dispPos
    bodySpr.setPosition(body.getDispPos());
    //explosionShape.setPosition(body.getDispPos());

    //sync rotation with dispVel
    if (body.getDispVel().x != 0 && body.getDispVel().y != 0)
    {
        rotation = std::atan(body.getDispVel().y / body.getDispVel().x)
                   / M_PI * 180;
        if (body.getDispVel().x < 0)
            rotation += 180;
    }
    if (rotation > 1e5) rotation = 90.0f;
    bodySpr.setRotation(rotation);
}

void Rocket::setGravityWind(const sf::Vector2f gravity, const sf::Vector2f wind)
{
    body.setGravityWind(gravity, wind);
}

void Rocket::addCollider(const MultiRing2f* collider)
{
    body.addCollider(collider);
}

void Rocket::setPosition(const sf::Vector2f& pos)
{
    body.setPosition(pos);
}

void Rocket::setVelocity(const sf::Vector2f& vel)
{
    body.setVelocity(vel);
}

const sf::Vector2f& Rocket::getPosition()const
{
    return body.getPos();
}

bool Rocket::hasCollision()const
{
    return body.hasCollision() || isInsideCollider;
}

bool Rocket::isFlyingLowerThan(float y)const
{
    return body.getPos().y > y;
}

sf::Shape& Rocket::getExplosionShape()const
{
    return *pExplosionShape;
}


}
