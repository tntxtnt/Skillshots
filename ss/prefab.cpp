#include "prefab.h"

namespace  ss
{

Prefab::Store Prefab::store;

Rocket Prefab::clone(Rocket::Type type)
{
    if (store.find(type) == end(store))
    {
        std::cerr << "No rocket of this type\n";
        exit(1);
    }
    return boost::get<Rocket>(store[type]);
}

bool Prefab::loadFromFile(Rocket::Type type, const std::string& filename)
{
    if (store.find(type) != end(store))
        return false;
    return boost::get<Rocket>(store[type] = Rocket{}).loadFromFile(filename);
}

Vehicle Prefab::clone(Vehicle::Type type)
{
    if (store.find(type) == end(store))
    {
        std::cerr << "No vehicle of this type\n";
        exit(1);
    }
    return boost::get<Vehicle>(store[type]);
}

bool Prefab::loadFromFile(Vehicle::Type type, const std::string& filename)
{
    if (store.find(type) != end(store))
        return false;
    return boost::get<Vehicle>(store[type] = Vehicle{}).loadFromFile(filename);
}

} //namespace ss
