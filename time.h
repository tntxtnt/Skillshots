#ifndef TIME_H
#define TIME_H

#include "headers.h"

class Time
{
public:
    /// Get time delta in seconds (time between 2 tick() calls).
    /// @return Time delta
    static float delta();

    /// Set time scale.
    ///   - =1 = normal time flow.
    ///   - >1 = faster time flow. (faster animations, movements, etc.)
    ///   - <1 = slower time flow. 0 = pause.
    /// @return None
    static void  scale(float);

    /// Get time scale.
    /// @return Time scale
    static float scale();

    /// Set number of fixed updates per second. Useful for making
    /// animations, movements, etc.  independent from FPS.
    /// @return None
    static void  fixedUpdatesPerSec(int);

    /// Get number of fixed updates per second. Useful for making
    /// animations, movements, etc.  independent from FPS.
    /// @return Number of fixed updates per second
    static int   fixedUpdatesPerSec();

    /// Get fixed time step (time as seconds between 2 fixed updates)
    /// @return Fixed time step
    static float fixedDelta();

    /// Restart inner clock.
    /// @return None
    static void  tick();
private:
    Time(){}
private:
    static float timeScale;
    static float timeDelta;
    static float timeFixedDelta;
    static int   nFixedUpdates;
    static sf::Clock timeClock;
};

#endif // TIME_H
