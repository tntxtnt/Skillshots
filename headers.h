#ifndef HEADERS_H
#define HEADERS_H

#define _USE_MATH_DEFINES
#include <cmath>
#include <cstdint>

#include <iostream>
#include <fstream>
#include <iomanip>

#include <string>
#include <vector>
#include <set>
#include <map>
#include <tuple>
#include <boost/variant.hpp>
#include <algorithm>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>

#endif
