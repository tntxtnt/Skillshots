#include "time.h"

int   Time::nFixedUpdates  = 50;
float Time::timeFixedDelta = 1.0f / Time::nFixedUpdates;
float Time::timeScale = 1;
float Time::timeDelta = 0;
sf::Clock Time::timeClock;

float Time::delta() { return timeDelta; }

float Time::fixedDelta() { return timeFixedDelta; }

float Time::scale() { return timeScale; }

void Time::scale(float f) { timeScale = f; }

int Time::fixedUpdatesPerSec() { return nFixedUpdates; }

void Time::fixedUpdatesPerSec(int n)
{
    nFixedUpdates = n;
    timeFixedDelta = 1.0f / nFixedUpdates;
}

void Time::tick()
{
    timeDelta = timeClock.restart().asSeconds() * timeScale;
}

