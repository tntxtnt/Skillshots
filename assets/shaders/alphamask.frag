uniform sampler2D texture;
uniform sampler2D masktex;

void main()
{
    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);
    vec4 mask = texture2D(masktex, gl_TexCoord[0].xy);
    pixel.a *= mask.r;
    gl_FragColor = pixel;
}
