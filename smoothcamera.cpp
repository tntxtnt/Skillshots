#include "smoothcamera.h"

void SmoothCamera::smoothMove()
{
    // k = (1 - lerpAmt)^(elapsedTime/timeDelta)
    // x0 = k*(x0 - xn) + xn
    //   - x0: camera center
    //   - xn: camera target
    auto moveDirection = getCenter() - target;
    if (std::fabs(moveDirection.x) + std::fabs(moveDirection.y) < snapThreshold)
        return setCenter(target);
    float k = std::pow(1.0f - lerpAmt, Time::delta()*Time::fixedUpdatesPerSec());
    setCenter(k*moveDirection + target);

    //Alternative without using std::pow
    /*timeSinceLastLerp += Time::delta();
    if (timeSinceLastLerp >= Time::step())
    {
        auto moveDirection = getCenter() - target;
        if (std::fabs(moveDirection.x) + std::fabs(moveDirection.y) < snapThreshold)
        {
            setCenter(target);
            timeSinceLastLerp = 0;
        }
        else
        {
            setCenter((1 - lerpAmt)*moveDirection + target);
            timeSinceLastLerp -= Time::step();
        }
    }*/
}

void SmoothCamera::handleMovement()
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        target.x -= mps * Time::delta();
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        target.x += mps * Time::delta();
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        target.y -= mps * Time::delta();
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        target.y += mps * Time::delta();
}

void SmoothCamera::setMovePerSec(float amt) { mps = amt; }

float SmoothCamera::getMovePerSec()const { return mps; }

void SmoothCamera::setLerp(float amt) { lerpAmt = amt; }

float SmoothCamera::getLerp()const { return lerpAmt; }

void SmoothCamera::zoomIn() { zoom(1/zoomAmt); }

void SmoothCamera::zoomOut() { zoom(zoomAmt); }
