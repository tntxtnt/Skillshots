#include "config.h"

Config Config::global;

Config::ImplicitVariantTypes::operator int32_t()const { return boost::get<int32_t>(val); }

Config::ImplicitVariantTypes::operator uint32_t()const { return boost::get<uint32_t>(val); }

Config::ImplicitVariantTypes::operator std::string()const { return boost::get<std::string>(val); }

Config::ImplicitVariantTypes::operator float()const { return boost::get<float>(val); }

Config::ImplicitVariantTypes::operator size_t()const { return boost::get<int32_t>(val); }

Config::ImplicitVariantTypes::operator bool()const { return boost::get<int32_t>(val); }

bool Config::loadFromFile(const std::string& filename)
{
    std::ifstream fin(filename);
    if (!fin) return false;

    std::string type, key, ignore;
    std::string s;
    int32_t n;
    float f;
    uint32_t u;
    while (fin >> type >> key >> ignore)
    {
        if (type == "int")
        {
            fin >> n;
            data[key] = n;
        }
        else if (type == "float")
        {
            fin >> f;
            data[key] = f;
        }
        else if (type == "color")
        {
            fin >> std::hex >> u >> std::dec;
            data[key] = u;
        }
        else //default = std::string
        {
            fin >> std::quoted(s);
            data[key] = s;
        }
    }
    return true;
}

Config::ImplicitVariantTypes Config::get(const std::string& key)const
{
    return ImplicitVariantTypes{ data.at(key) };
}

Config::VariantTypes& Config::operator[](const std::string& key)
{
    return data[key];
}
