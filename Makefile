# EXECUTABLES - DO NOT MODIFIED
CXX = g++ -std=c++1z
RM = rm -rf
MKDIR = mkdir -p

# HELPFUL COMMANDS
print-%  : ; @echo $* = $($*)

# PROJECT NAME - will be output executable name
PROJECT = SkillShots


# Include
INC_DIRS = 

# Linker
LIB_DIRS = 


# BUILD TARGETS - change your compile flags here
BUILD_RELEASE = Release
CFLAGS_RELEASE = -Wall -O2
LFLAGS_RELEASE = -s -lsfml-graphics -lsfml-window -lsfml-system
#-mwindows

# Add another build target here

# Switch your build target here
BUILD = $(BUILD_RELEASE)
CFLAGS = $(CFLAGS_RELEASE)
LFLAGS = $(LFLAGS_RELEASE)

# Modules
MODULES = . ss

# Output directories - DO NOT MODIFIED
OBJDIR = build/$(BUILD)/obj
EXEDIR = build/$(BUILD)/bin
DEPDIR = build/dependencies
EXE = $(EXEDIR)/$(PROJECT)
EXEWIN = build\$(BUILD)\bin\$(PROJECT).exe

$(EXEDIR):
	$(MKDIR) $(EXEDIR)

TEMPALL_OBJS_DIRS += $(foreach module, $(MODULES), $(OBJDIR)/$(module))
ALL_OBJS_DIRS = $(TEMPALL_OBJS_DIRS:%.=%)
$(ALL_OBJS_DIRS):
	$(MKDIR) $@

TEMP_ALL_DEPS_DIRS +=  $(foreach module, $(MODULES), $(DEPDIR)/$(module))
ALL_DEPS_DIRS = $(TEMP_ALL_DEPS_DIRS:%.=%)
$(ALL_DEPS_DIRS):
	$(MKDIR) $@

# .cpp, .h, .o, .d
RSRCS = $(foreach module, $(MODULES), $(wildcard $(module)/*.cpp))
RSRCS_NODOT = $(RSRCS:./%=%)
RDEPS = $(RSRCS_NODOT:%.cpp=$(DEPDIR)/%.d)
ROBJS = $(RSRCS_NODOT:%.cpp=$(OBJDIR)/%.o)

$(DEPDIR)/%.d: %.cpp | $(ALL_DEPS_DIRS)
	$(CXX) $< -MM -MT $(<:%.cpp=$(OBJDIR)/%.o) > $@
	$(CXX) $< -MM -MT $@ >> $@

-include $(RDEPS)

$(OBJDIR)/%.o: %.cpp | $(ALL_OBJS_DIRS)
	$(CXX) -c $(CFLAGS) $< -o $@
	
$(EXE): $(ROBJS) | $(EXEDIR)
	$(CXX) $(ROBJS) $(LFLAGS) -o $@
	
all: release

build: release

release: $(EXE)

all-pch: headers.h.gch release
	
headers.h.gch: headers.h
	$(CXX) -c $(CFLAGS) $< -o $@

run:
	cmd //c start cmd //k "$(EXEWIN) && pause && exit"

clean:
	$(RM) $(EXE)
	$(RM) $(ROBJS)
	
clean-dep:
	$(RM) $(RDEPS)

.PHONY: run all clean all-pch clean-dep build release
