#include "headers.h"
#include "smoothcamera.h"
#include "ss/world.h"

//const float FPS = 60.0f;

int main()
{
    Config::global.loadFromFile("config.txt");

    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;
    sf::RenderWindow window(sf::VideoMode(1000, 600), "SFML window",
                            sf::Style::Default, settings);
    //window.setFramerateLimit(FPS);
    window.setVerticalSyncEnabled(true); //use v-sync instead of fps

    SmoothCamera camera;
    camera.setSize(window.getSize().x, window.getSize().y);
    camera.setMovePerSec(500.0f);
    camera.setCenter(300.0f, 200.0f);
    camera.matchTargetWithCenter();
    window.setView(camera);

    // Origin
    sf::CircleShape origin(4);
    origin.setOrigin(origin.getRadius(), origin.getRadius());
    origin.setFillColor(sf::Color::Green);
    origin.setPosition(0, 0);

    // Camera target
    sf::CircleShape camTarget(4);
    camTarget.setOrigin(camTarget.getRadius(), camTarget.getRadius());
    camTarget.setFillColor(sf::Color::Magenta);
    camTarget.setPosition(camera.getTarget());

    //////////////// NEW GAME INITTIALIZATION ////////////////
    // Prefabs
    std::cout << "Loading prefabs...\n";
    if (!ss::Prefab::loadFromFile(ss::Vehicle::Type::America,
                                  Config::global.get("VEHICLE:america")))
    {
        std::cerr << "Cannot load America vehicle\n";
        return 1;
    }
    std::cout << "Vehicle - done!\n";
    if (!ss::Prefab::loadFromFile(ss::Rocket::Type::Default,
                             Config::global.get("ROCKET:default")))
    {
        std::cerr << "Cannot load default rocket\n";
        return 1;
    }
    std::cout << "Rockets - done!\n";

    // Game world
	ss::World world;
	world.setGravity(sf::Vector2f{ 0.0f, 200.0f });
	world.setSwitchDelay(Config::global.get("switchDelay"));

    std::cout << "Loading map...\n";
    if (!world.loadMap(Config::global.get("MAP:1")))
        return 1;
    std::cout << "Map - done!\n";

	{
		std::ifstream fin{ "v1_pos.txt" };
		float x, y; fin >> x >> y;
		world.addVehicle(ss::Vehicle::Type::America, sf::Vector2f{ x, y });
		world.addVehicle(ss::Vehicle::Type::America, sf::Vector2f{ x + 200, y - 100 });
		world.setActiveVehicle(0);
	}
    //////////// END - NEW GAME INITTIALIZATION ////////////

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyPressed)
            {
                switch (event.key.code)
                {
                case sf::Keyboard::I:
                    camera.zoomIn();
                    break;
                case sf::Keyboard::O:
                    camera.zoomOut();
                    break;
                default:
                    break;
                }
            }
            world.catchEvent(event);
        }

        Time::tick();
        world.update();
        world.handleEvents();

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) ||
            sf::Keyboard::isKeyPressed(sf::Keyboard::RControl))
        {
            camera.handleMovement();
        }
        else
        {
            camera.setTarget(world.getSelectedVehicle().displayPos());
        }
        camera.smoothMove();
        window.setView(camera);
        camTarget.setPosition(camera.getTarget());


        window.clear(sf::Color{0, 63, 127});
        window.draw(world);
        window.draw(origin);
        //window.draw(camTarget);
        window.display();
    }
}
